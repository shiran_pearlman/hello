import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  private path:string = 'https://firebasestorage.googleapis.com/v0/b/hello-85ed5.appspot.com/o'
  public images:string[] = [];

  constructor() {
    this.images[0] = this.path + 'buz.jpg' + '?alt=media&token=447304f7-2e94-4ae7-a51c-376fd53580ce';
    this.images[1] = this.path + 'entermnt.jpg' + '?alt=media&token=f2085031-f472-42be-bd09-7ae9032c54d8';
    this.images[2] = this.path + 'policits-icon.jpg' + '?alt=media&token=de1352b8-f55a-45ef-bf85-bd27f4042488';
    this.images[3] = this.path + 'sport.jpg' + '?alt=media&token=7d96457c-4916-4a2b-962e-956dfbef729a';
    this.images[4] = this.path + 'tech.jpg' + '?alt=media&token=a2e799cc-faae-4364-bb73-95f568d77609';

   }
}
