// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false, 
  firebaseConfig : {
    apiKey: "AIzaSyAV3pEnQfMjxD-9SgBCJ_kgUNpLHK0Jn4w",
    authDomain: "hello-85ed5.firebaseapp.com",
    databaseURL: "https://hello-85ed5.firebaseio.com",
    projectId: "hello-85ed5",
    storageBucket: "hello-85ed5.appspot.com",
    messagingSenderId: "419692029848",
    appId: "1:419692029848:web:ef42da86c1c8e9f6a3aded"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
